#include <iostream> //Wczytanie zależności
#include <cstdlib>
#include <iomanip>
#include <ctime>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.

/*
* Główna funkcja inicjalizująca program
*/
int main() {
	//Inicjalizacja stałych
	const int w = 15;
	const int k = 20;
	const int G = 20;
	int array[w][k]; //tablica zmiennych
	int divisibles_by_3[(k/2)]; //ilosc zmiennych podzielnych przez 3 w kolumnie
	int cache; //tymczasowa zmienna
	int random_value; //tymczasowa zmienna
	srand(time(NULL)); //wywołanie srand

	//Wypełnienie tablicy array[w][k] losowymi liczbami
	for(int i = 0; i < w; i++) { 
		for(int j = 0; j < k; j++) {
			random_value = rand()%(G + 1);
			array[i][j] = random_value;
		}
	}

	//Wyświetlenie tablicy
	for(int i = 0; i < w; i++) {
		for(int j = 0; j < k; j++) {
			cout << setw(5);
			cout << array[i][j];
			if(j == k-1) {
				cout << endl;
			}
		}
	}

	//Zabieg estetyczny
	for(int i = 0; i < k; i++) {
		cout << "=====";
	}

	cout << endl;

	//przypisanie poczatkowych wartosci do tablicy divisibles_by_3 rownych zawsze 0
	for(int i = 0; i < (k/2); i++) {
		divisibles_by_3[i] = 0;
	}

	//ustalenie ile razy w kolumnie wystapila liczba podzielna przez 3
	for(int i = 0; i < w; i++) {
		for(int j = 1; j < k; j += 2) {
			if(array[i][j]%3 == 0) {
				divisibles_by_3[(j/2)]++;
			}
		}
	}

	//Wypisanie ile razy w kolumnie wystapila liczba podzielna przez 3
	for(int i = 0; i < k; i++) {
		cout << setw(5);
		if(i%2 == 1) {
			cout << divisibles_by_3[i/2];
		} else {
			cout << "-";
		}
	}
	cout << endl;

	//odwrocenie kolumny tablicy jesli ilosc liczb podzielnych przez 3 w kolumnie jest wieksza niz 2
	for(int i = 1; i < k; i+=2) {
		if(divisibles_by_3[i/2] > 2) {
			for(int j = 0; j < (w/2); j++) {
				cache = array[j][i];
				array[j][i] = array[w-j-1][i];
				array[w-j-1][i] = cache;
			}
		}
	}

	//Zabieg estetyczny
	for(int i = 0; i < k; i++) {
		cout << "=====";
	}

	cout << endl;

	//Wypisanie tablicy drugi raz
	for(int i = 0; i < w; i++) {
		for(int j = 0; j < k; j++) {
			cout << setw(5);
			cout << array[i][j];
			if(j == k-1) {
				cout << endl;
			}
		}
	}

	return 0;
}